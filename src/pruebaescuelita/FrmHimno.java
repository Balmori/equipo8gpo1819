/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebaescuelita;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;

/**
 *
 * @author Yesenia
 */
public class FrmHimno extends JFrame{
    
 
    
    
    public FrmHimno(){
        marco();
    }
    
    
    public void marco(){
        
        
        JFrame f = new JFrame();
        f.setSize(600,600);
        f.setTitle("Himno y Escudo");
        f.setLocation(200,200);
        f.setVisible(true);
       

       
        JPanel panelMayor = new JPanel(new BorderLayout());
        
        JPanel panelNorte  = new JPanel();
        JPanel panelCentro = new JPanel();
        JPanel panelSur    = new JPanel();
        JPanel panelEste   = new JPanel();
        JPanel panelOeste  = new JPanel();
        
        //layout Managers a utilizar
       FlowLayout fl1=new FlowLayout(FlowLayout.CENTER);
       panelNorte.setLayout(fl1);
       
       GridBagLayout gbl1=new GridBagLayout();
       GridBagConstraints gbc1 = new GridBagConstraints();
       panelCentro.setLayout(gbl1);
       
       FlowLayout fl2=new FlowLayout();
       panelSur.setLayout(fl2);
       
       GridLayout gl2=new GridLayout(10, 20);
       panelEste.setLayout(gl2);
       
       GridLayout gl3=new GridLayout(10, 20);
       panelOeste.setLayout(gl3);
       
       //creamos compnentes para panel Norte
       JLabel lblTitulo=new JLabel("HIMNO");
       
       String[] lista={"Himno del Instituto Tecnológico de Veracruz\n",
 
"Letra: Francisco Rivera Ávila. Música: Agustín Lara y Aguirre\n" ,
"\n" ,
"Con ilusión, prestancia y con valor,\n" ,
"con juvenil deseo de vencer,\n" ,
"a mi Instituto voy con sin igual fervor,\n" ,
"en busca de la luz y del saber.\n" ,
"\n" ,
" Para servir mejor a la Nación,\n" ,
"cual corresponde a nuestra juventud,\n" ,
"estudio con tesón, medito con quietud,\n" ,
"y llevo como símbolo el honor.\n" ,
"\n" ,
" Tecnológico será\n" ,
"\"Antorcha y Luz de fuego permanente ®\",\n" ,
"que hará brillar tenaz y prepotente,\n" ,
"el nombre de mi Heróica Veracruz.\n" ,
"\n" ,
" Sonó el clarín y brilla un nuevo sol,\n" ,
"la juventud conquista un ideal,\n" ,
"con fé en el porvenir contempla el arrebol,\n" ,
"que cubre el panorama nacional.\n" ,
"\n" ,
" Para servir mejor a la Nación,\n" ,
"cual corresponde a nuestra juventud,\n" ,
"estudio con tesón, medito con quietud,\n" ,
"y llevo como símbolo el honor.\n" ,
"\n" ,
" Tecnológico será\n" ,
"\"Antorcha y Luz de fuego permanente ®\",\n" ,
"que hará brillar tenaz y prepotente,\n" ,
"el nombre de mi Heróica Veracruz."};
       
       JLabel lbl1=new JLabel("Contenido:");
       JList lis=new JList(lista);
       
    
      
       //componentes para panel sur
       JButton bs3=new JButton("Salir");
       
       
       bs3.setText("Salir");
       
       
       
       
       
      //agregado de componente a paneles
       panelNorte.add(lblTitulo);
       
       gbc1.gridx=0;
       gbc1.gridy=0;
       
       //gbc1.weighty=1.0;
       
       gbc1.anchor=GridBagConstraints.WEST;
       panelCentro.add(lbl1,gbc1);
       gbc1.gridy=1;
       gbc1.gridwidth=5;
       panelCentro.add(lis,gbc1);
      gbc1.gridwidth=1;
       gbc1.gridy=2;
     
        
         
         
        
       
       panelSur.add(bs3);
     
       
       
       
       //integrar paneles a paneles Mayor
       
      panelMayor.add(panelNorte, BorderLayout.NORTH);
      panelMayor.add(panelCentro, BorderLayout.CENTER);
      panelMayor.add(panelSur, BorderLayout.SOUTH);
      panelMayor.add(panelEste, BorderLayout.EAST);
      panelMayor.add(panelOeste, BorderLayout.WEST);
      
      //ASOCIAR EL PANEL MAYOR A LA FORMA O VENTANA
      f.add(panelMayor);
        
      getContentPane().setBackground(Color.RED);
     
        
    }
}
    

